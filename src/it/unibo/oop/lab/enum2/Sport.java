/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 */
public enum Sport {
	BASKET(Place.INDOOR, 10, "Basket"),
	VOLLEY(Place.OUTDOOR, 6, "Volley"),
	TENNIS(Place.OUTDOOR, 1, "Tennis"),
	BIKE(Place.OUTDOOR, 1, "Bike"),
	F1(Place.OUTDOOR, 1, "F1"),
	MOTOGP(Place.OUTDOOR, 1, "Moto GP"),
	SOCCER(Place.OUTDOOR, 11, "Soccer");

	private int teamSize;
	private String name;
	private Place place;

	private Sport(final Place place, final int noTeamMembers, final String actualName) {
		this.place = place;
		this.teamSize = noTeamMembers;
		this.name = actualName;
	}

	/**
	 * Checks if a sport is individual
	 * 
	 * @return true if the sport is individual, false otherwise
	 * */
	public boolean isIdividualSport() {
		return this.teamSize == 1;
	}

	/**
	 * Checks if the sport is done indoors
	 * 
	 * @return true if the sport is indoors, false otherwise
	 * */
	public boolean isIndoorSport() {
		return this.place == Place.INDOOR;
	}

	/**
	 * Gets where the sport is done
	 * 
	 * @return Where the sport is done as a {@code Place}
	 * */
	public Place getPlace() {
		return this.place;
	}
	
	public String toString() {
		return this.name;
	}
}
