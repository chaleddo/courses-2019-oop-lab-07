package it.unibo.oop.lab.nesting2;

import java.util.List;
import java.util.Objects;
import java.util.Iterator;

/**
 * Acceptable that checks elements over a list
 * 
 * @param <T>
 * 				The list element type
 * */
public class OneListAcceptable<T> implements Acceptable<T> {

	private List<T> list;
	
	/**
	 * Creates a new acceptable with an empty list
	 * */
	public OneListAcceptable() {
		this(List.of());
	}
	
	/**
	 * Creates a new acceptable with the specified list
	 * 
	 * @param from
	 * 				The list from which the acceptable will be created
	 * 
	 * @throws NullPointerException()
	 * */
	public OneListAcceptable(List<T> from) {
		this.list = Objects.requireNonNull(from);
	}

	/**
	 * Returns an acceptor based on the current list
	 * */
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			// The list iterator used for checking elements
			private Iterator<T> it;
			
			/**
			 * {@inheritDoc}
			 * */
			public void accept(T newElement) throws ElementNotAcceptedException {
				updateIterator();
				if(!this.it.hasNext() || (this.it.next() != newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
			}
			
			/**
			 * {@inheritDoc}
			 * */
			public void end() throws EndNotAcceptedException {
				updateIterator();
				if(this.it.hasNext()) {
					throw new EndNotAcceptedException();
				}
			}
			
			// Assigns the iterator reference if needed
			private void updateIterator() {
				if(this.it == null) {
					this.it = list.iterator();
				}
			}
		};
	}

}
